package main

import (
	"encoding/xml"
	"flag"
	"fmt"
	"time"

	"github.com/wuwz/goexec"
)

var (
	repos = []string{
		"http://svn-server:8082/svn/code/helpdesk",
		"http://svn-server.202:8082/svn/code/ewei-group",
	}
	limit      int
	author     string
	date       string
	dateLayout = "2006-01-02"
)

type XMLStruct struct {
	XMLName    xml.Name   `xml:"log"`
	LogEntries []LogEntry `xml:"logentry"`
}

type LogEntry struct {
	XMLName  xml.Name      `xml:"logentry"`
	Revision int           `xml:"revision,attr"`
	Author   string        `xml:"author"`
	Date     time.Time     `xml:"date"`
	Msg      string        `xml:"msg"`
	Paths    LogEntryPaths `xml:"paths"`
}

func (log *LogEntry) Valid(author, date string) bool {
	return log.Author == author && log.Date.Format(dateLayout) == date
}

type LogEntryPaths struct {
	XMLName xml.Name        `xml:"paths"`
	List    []LogoEntryPath `xml:"path"`
}

type LogoEntryPath struct {
	XMLName xml.Name `xml:"path"`
	Action  string   `xml:"action,attr"`
	Value   string   `xml:",innerxml"`
}

func init() {
	flag.IntVar(&limit, "limit", 100, "log limit")
	flag.StringVar(&author, "author", "wwz", "filter author")
	flag.StringVar(&date, "date", time.Now().Format(dateLayout), "filter date")
}

func main() {
	flag.Parse()

	var filteredLogs []LogEntry
	for _, repo := range repos {
		filteredLogs = append(filteredLogs, CrawlSvnLogWithFiltered(repo)...)
	}
	fmt.Printf("%s工作日志 \n", date)

	for i, log := range filteredLogs {
		fmt.Printf("%d) %s [SVN#%d] \n", i+1, log.Msg, log.Revision)
		for i, file := range log.Paths.List {
			fmt.Printf("\t∙[%s]%s\n", file.Action, file.Value)
			if i >= 4 {
				fmt.Printf("\t...\n")
				break
			}
		}
		fmt.Println()
	}
}

func CrawlSvnLogWithFiltered(repo string) []LogEntry {
	output, err := goexec.WithSync("svn log -v --xml %s -l %d  --username wwz --password wwz123456 ", repo, limit)
	if nil != err {
		panic(err)
	}
	var logEntries []LogEntry

	var result = XMLStruct{}
	if err := xml.Unmarshal([]byte(output), &result); err != nil {
		panic(err)
	}
	for _, log := range result.LogEntries {
		if log.Valid(author, date) {
			logEntries = append(logEntries, log)
		}
	}
	return logEntries
}
