local hotkey = require('core.hotkey')

hotkey.bindWithCtrlCmdAlt('R', '重新加载配置文件', function()
    hs.alert.show('加载配置文件中..')
    hs.timer.doAfter(0.1, function()hs.reload()end)
end)