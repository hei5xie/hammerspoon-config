local hotkey = require('core.hotkey')

hotkey.bindWithCtrlShift('L', "一键锁定屏幕", function()
    hs.execute("/System/Library/CoreServices/Menu\\ Extras/User.menu/Contents/Resources/CGSession -suspend")
end)