local hotkey = require('core.hotkey')
local function move(x, y, w, h)
    return function()
        win = hs.window.focusedWindow()
        if win then
            win_f = win:frame()
            screen_f = win:screen():frame()
            print('input: x='..x..',y='..y..',w='..w..',h='..h)
            print('screen: x='..screen_f.x..',y='..screen_f.y..',w='..screen_f.w..',h='..screen_f.h)
            print('begin_window: x='..win_f.x..',y='..win_f.y..',w='..win_f.w..',h='..win_f.h)
            print('win_f.x = screen_f.w * x + screen_f.x -> '.. screen_f.w * x + screen_f.x)
            print('win_f.y = screen_f.h * y -> '.. screen_f.h * y)
            print('win_f.w = screen_f.w * w -> '.. screen_f.w * w)
            print('win_f.h = screen_f.h * h -> '.. screen_f.h * h)

            win_f.x = screen_f.w * x + screen_f.x
            win_f.y = screen_f.h * y
            win_f.w = screen_f.w * w
            win_f.h = screen_f.h * h

            print('end_window: x='..win_f.x..',y='..win_f.y..',w='..win_f.w..',h='..win_f.h)
            win:setFrame(win_f, 0)
        end
    end
end

hotkey.bindWithCtrlShift('Up', '[窗口管理]向上移动窗口', move(0, 0, 1, 0.5))
hotkey.bindWithCtrlShift('Right', '[窗口管理]向右移动窗口', move(0.5, 0, 0.5, 1))
hotkey.bindWithCtrlShift('Down', '[窗口管理]向下移动窗口', move(0, 0.5, 1, 0.5))
hotkey.bindWithCtrlShift('Left', '[窗口管理]向左移动窗口', move(0, 0, 0.5, 1))
hotkey.bindWithCtrlShift('M', '[窗口管理]最大化窗口', move(0, 0, 1, 1))
hotkey.bindWithCtrlShift('C', '[窗口管理]居中窗口', move(0.05, 0.08, 0.9, 0.9))
hotkey.bindWithCmdAlt('Left', '[窗口管理]向左上角移动窗口', move(0, 0, 0.5, 0.5))
hotkey.bindWithShiftCmdAlt('Left', '[窗口管理]向左下角移动窗口', move(0, 0.5, 0.5, 0.5))
hotkey.bindWithCmdAlt('Right', '[窗口管理]向右上角移动窗口', move(0.5, 0, 0.5, 0.5))
hotkey.bindWithShiftCmdAlt('Right', '[窗口管理]向右下角移动窗口', move(0.5, 0.5, 0.5, 0.5))
