# Hammerspoon Config
一个方便好用的自用的[Hammerspoon](http://www.hammerspoon.org/)配置文件。
> hammerspoon一款macOS平台的自动化工具，可以使用Lua脚本控制你的电脑，做你想做的任何事情。

# 配套博文
https://wuwenze.com/article/vasetn.html

<a name="6YP6B"></a>
# Install
```bash
brew install hammerspoon

cd ~/.hammerspoon && rm -vrf *

git clone https://gitee.com/wuwenze/hammerspoon-config .

# 重启hammerspoon
```

<a name="Core"></a>
# Core

- `core/hotkey.lua`: 将所有通过Hammerspoon注册的快捷键管理起来，并提供方便的绑定函数。
> 使用快捷键：`CTRL`+`CMD`+`ALT`+`K` 显示目前所有可用的快捷键

```bash
▶︎ (Ctrl+Cmd+Alt+K) ☞显示所有快捷键
▶︎ (Ctrl+Cmd+Alt+R) ☞重新加载配置文件
▶︎ (Ctrl+Cmd+Alt+E) ☞编辑配置文件
▶︎ (Ctrl+Shift+H) ☞编辑Hosts文件
▶︎ (Ctrl+Shift+L) ☞一键锁定屏幕
▶︎ (Ctrl+Shift+Up) ☞[窗口管理]向上移动窗口
▶︎ (Ctrl+Shift+Right) ☞[窗口管理]向右移动窗口
▶︎ (Ctrl+Shift+Down) ☞[窗口管理]向下移动窗口
▶︎ (Ctrl+Shift+Left) ☞[窗口管理]向左移动窗口
▶︎ (Ctrl+Shift+M) ☞[窗口管理]最大化窗口
▶︎ (Ctrl+Shift+C) ☞[窗口管理]居中窗口
▶︎ (Cmd+Alt+Left) ☞[窗口管理]向左上角移动窗口
▶︎ (Shift+Cmd+Alt+Left) ☞[窗口管理]向左下角移动窗口
▶︎ (Cmd+Alt+Right) ☞[窗口管理]向右上角移动窗口
▶︎ (Shift+Cmd+Alt+Right) ☞[窗口管理]向右下角移动窗口
....
```

![image.png](https://cdn.nlark.com/yuque/0/2019/png/243237/1556254823992-01644d1f-0e47-4cf2-9b18-7359716b4737.png#align=left&display=inline&height=719&name=image.png&originHeight=1438&originWidth=2558&size=6180225&status=done&width=1279)

<a name="Modules"></a>
# Modules

- `modules/config.lua`：按下`CTRL`+`CMD`+`ALT`+`E`，直接编辑Hammerspoon配置文件（需先安装TextMate）

- `modules/reload.lua`：按下`CTRL`+`CMD`+`ALT`+`R`，重新加载Hammerspoon配置文件。

- `modules/hosts.lua`：按下`CTRL`+`CMD`+`ALT`+`H`，直接编辑/etc/hosts配置文件（需先安装TextMate）

- `modules/lockscreen.lua`：按下`CTRL`+`SHIFT`+`L`，一键锁屏。

- `modules/memory.lua`：内存监控小插件，在全局菜单栏上显示当前的内存使用情况。

      ![Jietu20190425-213726-HD.gif](https://cdn.nlark.com/yuque/0/2019/gif/243237/1556199473567-8ae7cc91-c96e-4788-933c-e1d9548267c4.gif#align=left&display=inline&height=89&name=Jietu20190425-213726-HD.gif&originHeight=89&originWidth=599&size=1530983&status=done&width=599)<br />

- `modules/speed.lua`：网速监控小插件，在全局菜单栏上显示当前的网络使用情况。

      ![Jietu20190425-213952-HD.gif](https://cdn.nlark.com/yuque/0/2019/gif/243237/1556199628663-1e2398c9-c73e-46e3-b05d-089863127ae5.gif#align=left&display=inline&height=66&name=Jietu20190425-213952-HD.gif&originHeight=66&originWidth=599&size=2098415&status=done&width=599)

- `modules/weather.lua`：天气预报小插件，在全局菜单栏上显示本地未来七天的天气预报。

      ![image.png](https://cdn.nlark.com/yuque/0/2019/png/243237/1556199714274-50a04b65-c6f2-4854-a81f-ee9b79689fd6.png#align=left&display=inline&height=216&name=image.png&originHeight=432&originWidth=962&size=333280&status=done&width=481)

- `modules/window.lua`：窗口管理

     ![1123123123.gif](https://cdn.nlark.com/yuque/0/2019/gif/243237/1556256233908-6849e715-e2bb-40b1-8d40-966d73eed2be.gif#align=left&display=inline&height=283&name=1123123123.gif&originHeight=235&originWidth=419&size=7758117&status=done&width=504)

- `private/*.lua`：私有脚本，只对本人有用的一些辅助功能。


<a name="8DOGg"></a>
# 说明
部分实现思路来自其他大神的脚本(已在源码中注明来源)

<a name="N6Um6"></a>
# 后续计划

- 聪明的Dock
- ...
