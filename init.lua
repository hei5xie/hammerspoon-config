require('modules.default')
require('modules.reload')
require('modules.config')
require('modules.hosts')
require('modules.lockscreen')
--require('modules.weather')
require('modules.speed')
require('modules.memory')
require('modules.window')
require('modules.switcher')

--// 加载私有模块
if (string.find(hs.execute('whoami'), 'wuwenze') ~= nil) then
  require('private.helpdesk')
	require('private.cdock')
	require('private.ngrok')
end
