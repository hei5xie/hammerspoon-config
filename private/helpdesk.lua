local exportEnvCommand = 'export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_181.jdk/Contents/Home && export M2_HOME=/Users/wuwenze/Development/apache-maven-3.6.0  && export logFileRoot=/Users/wuwenze/Development/ewei-logs && export PATH=$JAVA_HOME/bin:$M2_HOME/bin:$PATH && export PATH=$PATH:/usr/local/mysql/bin &&'

local hotkey = require('core.hotkey')

local function installSdk()
	cdProjectCommand = 'cd /Users/wuwenze/Development/ewei-projects/helpdesk &&'
	
	hs.alert.show('Installing ewei-open-sdk...')
	hs.timer.doAfter(0.1, function() 
		hs.execute(exportEnvCommand..cdProjectCommand..' ./helpdesk.sh install-sdk && say installSdk success')
	end)
end
hotkey.bindWithShiftCmd('I', '[私有]编译OpenSdk', installSdk)


local function builderWorklog() 
	bin = '~/.hammerspoon/bin/helpdesk/worklog'
	exampleParam = '-limit 100 -date '..os.date('%Y-%m-%d')
	
	_, param = hs.dialog.textPrompt("生成工作日志", "示例："..exampleParam, exampleParam, "开始生成")
	if param ~= '' then
		hs.alert.show('生成工作日志中...')
		hs.timer.doAfter(0.5, function()
			result = hs.execute(exportEnvCommand.. bin ..' '..param..' && say 生成工作日志成功')
			hs.alert.closeAll()
			option = hs.dialog.blockAlert("生成结果", result, "编辑", "复制到剪切板")
			if option == '复制到剪切板' then
				hs.pasteboard.setContents(result)
			end
			if option == '编辑' then
				tmpfile = '/tmp/'..os.date("%Y%m%d%H%M%S", os.time())..'.log';
				hs.execute('echo "'..result..'" > '..tmpfile)
				hs.execute('/usr/local/bin/mate '..tmpfile)
			end
		end)
	end
end
hotkey.bindWithShiftCmd('W', '[私有]生成工作日志', builderWorklog)