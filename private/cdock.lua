-- 解决cDock在macOS 10.14.4上不生效的问题
if (string.find(hs.execute("sw_vers | awk 'NR==2 {print $2}'"), '10.14.4') ~= nil) then
	hs.timer.doAfter(2, function()
		hs.execute('nohup /Applications/cDock.app/Contents/MacOS/cDock > /tmp/cdock.log 2>&1 &')
		hs.execute('killall Dock && killall cDock')
	end)
end
