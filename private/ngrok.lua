local hotkey = require('core.hotkey')

hotkey.bindWithShiftCmd('N', '[私有]启动Ngrok内网穿透', function() 
	exampleParam = '-subdomain wuwz 80'
	
	_, param = hs.dialog.textPrompt("启动Ngrok", "示例："..exampleParam, exampleParam, "确定")
	if param ~= '' then
		hs.osascript.applescript('tell application "Terminal"\n'..
		'	set newTab to do script "cd /Users/wuwenze/.hammerspoon/bin/ngrok"\n'..
		'	do script "./ngrok -config ngrok.conf '..param..'" in front window\n'..
		'end tell')
	end
end)